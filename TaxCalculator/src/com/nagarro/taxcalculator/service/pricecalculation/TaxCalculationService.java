/*
* Class name: TaxCalculationService
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 11/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: TaxPriceCalculation used for calculate tax price
*/
package com.nagarro.taxcalculator.service.pricecalculation;

public interface TaxCalculationService {
    /**
     * Calculate the tax price of item
     * 
     * @param itemPrice
     * @param itemQuantity
     * @return return tax price
     */
    double taxPrice(double itemPrice, int itemQuantity);
}
