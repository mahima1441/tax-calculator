/*
* Class name: ImportedTaxPriceServiceImpl
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 11/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: Calculate the tax price for the imported item type
*/
package com.nagarro.taxcalculator.service.pricecalculation.impl;

import com.nagarro.taxcalculator.service.pricecalculation.TaxCalculationService;

public class ImportedTaxPriceServiceImpl implements TaxCalculationService {
    /**
     * Calculate the tax price of Imported item type
     * 
     * @param itemPrice
     * @param itemQuantity
     * @return return tax price of Imported item
     */
    @Override
    public double taxPrice(double itemPrice, int itemQuantity) {
        double importedTax = itemPrice * .01;
        if (importedTax <= 100) {
            importedTax += 5;
        } else if (importedTax >= 100 && importedTax <= 200)
            importedTax += 10;
        else
            importedTax += 5.0 / 100 * (importedTax + itemPrice);
        return importedTax;
    }
}
