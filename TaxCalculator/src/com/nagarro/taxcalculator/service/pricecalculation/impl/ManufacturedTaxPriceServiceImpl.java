/*
* Class name: ManufacturedTaxPriceServiceImpl
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 11/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: Calculate the tax price for the manufactured item type
*/
package com.nagarro.taxcalculator.service.pricecalculation.impl;

import com.nagarro.taxcalculator.service.pricecalculation.TaxCalculationService;

public class ManufacturedTaxPriceServiceImpl implements TaxCalculationService {
    /**
     * Calculate the tax price of Manufacture item type
     * 
     * @param itemPrice
     * @param itemQuantity
     * @return return tax price of Manufactured item
     */
    @Override
    public double taxPrice(double itemPrice, int itemQuantity) {
        double manufacturedTax = itemPrice * .125;
        manufacturedTax += (itemPrice + manufacturedTax) * .02;
        return manufacturedTax;
    }
}
