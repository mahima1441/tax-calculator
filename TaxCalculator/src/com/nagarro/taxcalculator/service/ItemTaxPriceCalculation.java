/*
* Class name: ItemTaxPriceCalculation 
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 12/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: Calculate the tax price according to the item type
*/
package com.nagarro.taxcalculator.service;

import java.util.ArrayList;

import com.nagarro.taxcalculator.model.Item;
import com.nagarro.taxcalculator.service.pricecalculation.TaxCalculationService;
import com.nagarro.taxcalculator.service.pricecalculation.impl.ImportedTaxPriceServiceImpl;
import com.nagarro.taxcalculator.service.pricecalculation.impl.ManufacturedTaxPriceServiceImpl;
import com.nagarro.taxcalculator.service.pricecalculation.impl.RawTaxPriceServiceImpl;

public class ItemTaxPriceCalculation {

    /**
     * Calculate the tax according to the item type
     * 
     * @param itemsInfo
     * @return
     */
    public void calculateTax(ArrayList<Item> itemsInfo) {
        for (Item item : itemsInfo) {
            String type = item.getItemType();
            double itemsPrice = item.getItemPrice();
            int itemsQuantity = item.getItemQuantity();
            TaxCalculationService taxPriceCalculation = null;
            switch (type) {
            case "Raw": // Calculate Tax price for a Raw item type
                taxPriceCalculation = new RawTaxPriceServiceImpl();
                break;
            case "Manufactured": // Calculate Tax price for a Raw item type
                taxPriceCalculation = new ManufacturedTaxPriceServiceImpl();
                break;
            case "Imported": // Calculate Tax price for a Imported item type
                taxPriceCalculation = new ImportedTaxPriceServiceImpl();
                break;
            }
            double taxPrice = taxPriceCalculation.taxPrice(itemsPrice, itemsQuantity);
            item.setTaxPrice(taxPrice);
        }
    }
}
