/*
* Class name: InvalidItemException
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 08/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: give description of the Exceptions and provide user to re-enter the item information
*/
package com.nagarro.taxcalculator.exception;

public class InvalidItemException extends RuntimeException {


    private static final String ERROR = "error: ";

    /**
     * This method is used to give the error message if any exception occurs
     * 
     * @param exceptionMessage
     */
    public InvalidItemException(String exceptionMessage) {
        System.out.println(ERROR + exceptionMessage);
    }
}
