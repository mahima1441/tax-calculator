/*
* Class name: TaxCalculation
*
* Version info: jdk 1.8
*
* Copyright notice:
* 
* Author info: Mahima
*
* Creation date: 08/Apr/2021
*
* Last updated By: Mahima
*
* Last updated Date: 12/Apr/2021
*
* Description: Main class
*/
package com.nagarro.taxcalculator.main;

import com.nagarro.taxcalculator.utils.ItemManager;

public class TaxCalculation {
    /**
     * @param args
     */
    public static void main(String[] args) {
        new ItemManager().startManager();
    }
}
